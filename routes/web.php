<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', array('as'=>'home','uses'=>'HomeController@index'));
Route::get('/compare', array('as'=>'compare','uses'=>'HomeController@compare'));
Route::get('/faq', array('as'=>'faq','uses'=>'HomeController@faq'));


Route::get('page/{slug}/preview', array('as'=>'pagePreview','uses'=>'HomeController@preview'));
Route::get('/seo', 'HomeController@seo');