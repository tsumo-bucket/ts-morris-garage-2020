@extends('app.home')

@section('list')
<div class="list-grid category-list">
    <div class="list-item">
        <div class="product-card">
            <div class="card-image">
                <img src="{{ asset('img/app/MG_5-Alpha-Carbon Gray.png') }}" alt="">
            </div>
            <div class="card-title">
                <span class="model">MG 6</span>
                <span class="name">AT Alpha</span>
            </div>
            <div class="card-blurb">
                Big In Features, Big In Comfort
            </div>
            <hr>
            <div class="card-footer">
                <a href="?model=mg5" class="btn btn-primary btn-lg">View Available Trims</a>
            </div>
        </div>
    </div>
    <div class="list-item">
        <div class="product-card">
            <div class="card-image">
                <img src="{{ asset('img/app/MG_5-Alpha-Carbon Gray.png') }}" alt="">
            </div>
            <div class="card-title">
                <span class="model">MG 6</span>
                <span class="name">AT Alpha</span>
            </div>
            <div class="card-blurb">
                Big In Features, Big In Comfort
            </div>
            <hr>
            <div class="card-footer">
                <a href="?model=mg5" class="btn btn-primary btn-lg">View Available Trims</a>
            </div>
        </div>
    </div>
    <div class="list-item">
        <div class="product-card">
            <div class="card-image">
                <img src="{{ asset('img/app/MG_5-Alpha-Carbon Gray.png') }}" alt="">
            </div>
            <div class="card-title">
                <span class="model">MG 6</span>
                <span class="name">AT Alpha</span>
            </div>
            <div class="card-blurb">
                Big In Features, Big In Comfort
            </div>
            <hr>
            <div class="card-footer">
                <a href="?model=mg5" class="btn btn-primary btn-lg">View Available Trims</a>
            </div>
        </div>
    </div>
</div>
@endsection