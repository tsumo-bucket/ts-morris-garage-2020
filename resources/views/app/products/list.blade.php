@extends('app.home')

@section('list')
    <div class="result-options">
        <div class="tags">
            <span href="#" class="btn btn-secondary btn-tag btn-font-medium">{{ $request['model'] }} <i class="fal fa-times"></i></span>
        </div>
        <div class="actions">
            <button class="btn btn-primary btn-primary-outline btn-font-medium" id="compareBtn">Compare Units</button>
            <button class="btn btn-primary btn-primary-outline btn-dropdown btn-font-medium" type="button" id="dropDownPriceSort"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span>Lowest Price First</span> <i class="fas fa-chevron-down"></i>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropDownPriceSort">
                <div class="dropdown-item" data-value="MG 5">Lowest Price First</div>
                <div class="dropdown-item" data-value="MG 5">Highest Price First</div>
            </div>
        </div>
    </div>
    <div class="list-grid">
        @foreach ($list as $key)
            <div class="list-item" data-id="{{ $key }}">
                <div class="product-card">
                    <div class="overlayed-checkbox"></div>
                    <div class="card-image">
                        <img src="{{ asset('img/app/MG_5-Alpha-Carbon Gray.png') }}" alt="">
                    </div>
                    <div class="card-title">
                        <span class="model">MG 6</span>
                        <span>AT Alpha</span>
                    </div>
                    <div class="card-blurb">
                        Big In Features, Big In Comfort
                    </div>
                    <div class="accordion">
                        <div class="card">
                            <div class="card-header" id="{{ $key }}-headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseOne-{{ $key }}" aria-expanded="false"
                                        aria-controls="collapseOne-{{ $key }}">
                                        Vehicle Overview
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne-{{ $key }}" class="collapse" aria-labelledby="{{ $key }}-headingOne">
                                <div class="card-body">
                                    <p><strong>Model:</strong> 2020</p>
                                    <p><strong>Available Colors:</strong></p>
                                    <div class="colors">
                                        <span class="color" style="background: #FFF;"
                                            data-img="{{ asset('img/app/redimage.png') }}"></span>
                                        <span class="color" style="background: #000;"
                                            data-img="{{ asset('img/app/MG_5-Alpha-Carbon Gray.png') }}"></span>
                                        <span class="color" style="background: grey;"
                                            data-img="{{ asset('img/app/MGZS.png') }}"></span>
                                        <span class="color" style="background: red;"
                                            data-img="{{ asset('img/app/redimage.png') }}"></span>
                                        <span class="color" style="background: gold;"
                                            data-img="{{ asset('img/app/MG_6.png') }}"></span>
                                        <span class="color" style="background: purple;"
                                            data-img="{{ asset('img/app/MGRX5.png') }}"></span>
                                    </div>
                                    <p><strong>Transmission:</strong> Manual</p>
                                    <p><strong>Engine Size:</strong> 1.5L</p>
                                    <p><strong>Seats:</strong> 5</p>
                                    <p><strong>Drive:</strong> Rear wheel drive</p>
                                    <p><strong>Price:</strong> PHP 658,888.00</p>
                                    <p><i class="fas fa-images"></i> <a href="#">View gallery</a></p>
                                    <p><i class="fas fa-info-circle"></i> <a href="#">Learn more</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="{{ $key }}-headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseTwo-{{ $key }}" aria-expanded="false"
                                        aria-controls="collapseTwo-{{ $key }}">
                                        Key Features
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo-{{ $key }}" class="collapse" aria-labelledby="{{ $key }}-headingTwo">
                                <div class="card-body">
                                    <p><img src="{{ asset('img/app/Group 71.svg') }}" alt=""> Two-tone, ten-spoke, 16-inch alloy wheels</p>
                                    <p><img src="{{ asset('img/app/Group 70.svg') }}" alt=""> Powerful LED headlights</p>
                                    <p><img src="{{ asset('img/app/Group 72.svg') }}" alt=""> 10-inch touchscreen infotainment system</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="{{ $key }}-headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseThree-{{ $key }}" aria-expanded="false"
                                        aria-controls="collapseThree-{{ $key }}">
                                        Experience
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree-{{ $key }}" class="collapse" aria-labelledby="{{ $key }}-headingThree">
                                <div class="card-body">
                                    <p><img src="{{ asset('img/app/Group 19.svg') }}" alt="">360° view</a></p>
                                    <p><img src="{{ asset('img/app/Group 20.svg') }}" alt="">View walkaround video</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="{{ $key }}-heading4">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapse4-{{ $key }}" aria-expanded="false"
                                        aria-controls="collapse4-{{ $key }}">
                                        Links
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse4-{{ $key }}" class="collapse" aria-labelledby="{{ $key }}-heading4">
                                <div class="card-body">
                                    <p><img src="{{ asset('img/app/Path 56.svg') }}" alt="">Book a test drive</a></p>
                                    <p><img src="{{ asset('img/app/Group 17.svg') }}" alt="">Download brochure</a></p>
                                    <p><img src="{{ asset('img/app/Group 18.svg') }}" alt="">View offers</a></p>
                                    <p><img src="{{ asset('img/app/Path 167.svg') }}" alt="">View motoring reviews</a></p>
                                    <p><img src="{{ asset('img/app/Group 107.svg') }}" alt="">View customer reviews</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="btn btn-primary btn-lg show-inquire-modal">Inquire</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div id="compareOverlay" class="mg-modal-overlay">
        <div class="container">
            <div class="overlay-header">
                <button type="button" class="close">Close</button>
            </div>
            <div class="overlay-body overlay-body-placeholder">
                <div class="grid-item item-link-add">
                    <div class="default">
                        <i class="far fa-plus"></i> Add model
                    </div>
                </div>
                <div class="grid-item item-link-add">
                    <div class="default">
                        <i class="far fa-plus"></i> Add model
                    </div>
                </div>
                <div class="grid-item item-link-add">
                    <div class="default">
                        <i class="far fa-plus"></i> Add model
                    </div>
                </div>
            </div>
            <div class="overlay-body">

            </div>
            <div class="overlay-footer">
                <div class="grid-offset"></div>
                <div class="grid-item">
                    <a href="{{ route('compare') }}?model=mg5" class="btn btn-primary ">Compare Units</a>
                    <p>Select two to three units for comparison</p>
                </div>
                <div class="grid-offset"></div>
            </div>
        </div>
    </div>
@endsection
@section('added-scripts')
    @parent
    <script id="unitSelected" type="text/x-handlebars-template">
        <div class="grid-item" data-id="@{{ id }}">
            <div class="card-item">
                <button class="remove-item" type="button"><i class="far fa-times"></i></button>
                <div class="card-image">
                    <img src="{{ asset('img/app/MG_5-Alpha-Carbon Gray.png') }}" alt="">
                </div>
                <div class="card-title">
                    <span class="model">MG 6</span>
                    <span>AT Alpha</span>
                </div>
            </div>
        </div>
    </script>
    <script>
        $(document).ready(function() {
            let unitsToCompare = [];
            let isChoosing = false;
            var compareUnitTemplate = Handlebars.compile($("#unitSelected").html());

            $('.list-item .colors .color').hover(function(e) {
                var parent = $(this).closest('.list-item');
                var img = $(this).data('img');
                parent.find('.card-image img').attr('src', img);
            });
            $('#compareBtn').on('click', function(e) {
                e.preventDefault();
                $('#compareOverlay').addClass("shown");
                initSelectedUnitsToCompare();
            });
            $('#compareOverlay .close').on('click', function() {
                $('#compareOverlay').removeClass("shown");
                toggleChoosing(false);
            });
            $('#compareOverlay .item-link-add').on('click', function() {
                toggleChoosing(true);
            });
            $('#compareOverlay').on('click', '.card-item .remove-item', function(e) {
                e.preventDefault();
                var id = $(this).closest('.grid-item').data('id');
                console.log(id);
                const index = unitsToCompare.indexOf(id);
                if (index > -1) {
                    unitsToCompare.splice(index, 1);
                    $('.overlay-body:not(.overlay-body-placeholder)').find('.grid-item[data-id="' +
                        id + '"]').remove();
                    $('.list-item[data-id="' + id + '"] .product-card').removeClass('selected');
                    $('.cars-list').toggleClass('choosing-full', unitsToCompare.length >= 3);
                    initSelectedUnitsToCompare();
                }
            });

            function toggleChoosing(toggledChoosing) {
                isChoosing = toggledChoosing;
                if (isChoosing) {
                    $('#compareOverlay').addClass("choosing");
                    $('.cars-list').addClass("choosing");
                } else {
                    resetSelected();
                }
            }

            $('.cars-list .list-item').on('click', function(e) {
                var id = $(this).data('id');
                if (isChoosing) {
                    if (!unitsToCompare.includes(id)) {
                        if (unitsToCompare.length < 3) {
                            unitsToCompare.push(id);
                            console.log(unitsToCompare);
                            $('.overlay-body:not(.overlay-body-placeholder)').append(compareUnitTemplate({
                                id: id
                            }));
                            $(this).find('.product-card').addClass('selected')
                        }
                    } else {
                        const index = unitsToCompare.indexOf(id);
                        if (index > -1) {
                            unitsToCompare.splice(index, 1);
                            $('.overlay-body:not(.overlay-body-placeholder)').find('.grid-item[data-id="' +
                                id + '"]').remove();
                        }
                        $(this).find('.product-card').removeClass('selected')
                    }
                    $('.cars-list').toggleClass('choosing-full', unitsToCompare.length >= 3);
                    initSelectedUnitsToCompare();
                }

            });

            function resetSelected() {
                unitsToCompare = [];
                $('#compareOverlay').removeClass("choosing");
                $('.cars-list').removeClass("choosing");
                $('.cars-list').removeClass("choosing-full");
                $('.product-card').removeClass('selected');
                $('.overlay-body:not(.overlay-body-placeholder) .grid-item').remove();
            }

            function initSelectedUnitsToCompare() {
                console.log(initSelectedUnitsToCompare);
                if (unitsToCompare.length == 3) {
                    $('.overlay-body:not(.overlay-body-placeholder)').css('display', 'grid');
                } else {
                    $('.overlay-body:not(.overlay-body-placeholder)').css('display', 'none');
                }
            }
        });

    </script>
@endsection
