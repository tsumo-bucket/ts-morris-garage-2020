<footer>
    <div class="container footer-grid">
        <div class="grid-item">
            <h6>MG Philippines - The Covenant Car Company, Inc. </h6>
            <p>Ground Floor, ALCO Building, 391 Sen. Gil Puyat Avenue, Makati City 1200, Metro Manila Call MG (02)
                5328-4664</p>
        </div>
        <div class="grid-item">
            <p>Specifications and colors may vary from actual vehicle unit. Prices of regional dealers may vary due to
                freight cost.</p>
        </div>
        <div class="grid-item">
            <div class="social-items">
                <h6>Follow MG Philippines</h6>
                <div class="social-icons">
                    <a href="#">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a href="#">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a href="#">
                        <i class="fab fa-instagram"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>
