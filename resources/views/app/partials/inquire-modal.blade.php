<div class="modal mg-modal fade" id="inquireModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    Close
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">A Sales Consultant will contact you for an e-presentation
                    on your selected vehicle.</h4>
                <form action="#">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" name="firstname" placeholder="First Name" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" name="lastname" placeholder="Last Name" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" name="contact" placeholder="Contact No." class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" name="email" placeholder="Email Address" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <select name="dealer" class="form-control">
                                    <option value="1">MG EDSA Centris</option>
                                    <option value="1">MG BF Parañaque </option>
                                    <option value="1">MG Dasmariñas </option>
                                    <option value="1">MG Sta. Rosa</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <p>By clicking “I Agree” to TCCCI - MG Philippines’ privacy policies, you acknowledge that
                                you
                                have read, understood, and freely agreed to our privacy policy, and consent to the
                                collection, use, and processing of your personal data. You can opt-out at any time.</p>
                            <p> The data
                                that we collect from you may be transferred to, stored and processed by staff working
                                for us
                                or one of our suppliers. We will take all steps reasonably necessary to ensure that your
                                data is treated securely and in accordance with current laws.</p>
                            <div class="form-group form-checkbox">
                                <input type="checkbox" name="agree" class="form-control">
                                <label for="agree">I agree</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </div>
</div>
