@extends('layouts.app-amp')

@section('head')
    <link rel="canonical" href="{{route('appArticleView',@$slug)}}">
@stop

@section('content')
<article>
    <h1 class="montserrat-light">{{$blog->name}}</h1>
    <div class="details montserrat-light">
        <span>{{ \Carbon\Carbon::parse($blog->date)->format('F d, Y') }}</span>
    </div>
    
    <div class="banner montserrat-light">
        @if (@$blog->asset->path)
        <amp-img src="{{asset(@$blog->asset->path)}}" width="266" height="150" layout="responsive" alt="{{$blog->name}}"></amp-img>
        @endif
    </div>

    <div class="content">
        {!!$blog->amp_content!!}
    </div>
    
</article>
@stop