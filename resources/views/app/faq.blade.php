@extends('layouts.app')

@section('content')
<section class="banner-heading">
    <h4>
        {{ asset('img/app/fCCHBT.tif.svg') }}
        Frequently Asked Questions
    </h4>
    <p>Learn more about the Click n’ Drive shopping process from unit availability to returns.</p>
    <p>For more inquiries, contact us through our 24/7 MG Hotline (02) 5328-4664, social media platforms, or the My MG App.</p>
</section>
<section class="faq-body">
    <div class="faq-group">
        <div class="question">
            <img src="" alt="">
        </div>
    </div>
</section>
@endsection