@extends('layouts.app')

@section('content')
<section id="section-compare">
    <div class="container">
        <div class="compare-header">
            <a href="{{ route('home') }}?model=mg5" type="button" class="close">Close</a>
        </div>
        <div class="compare-body">
            <div></div>
            <div class="grid-item" data-id="0">
                <div class="card-item">
                    <a href="{{ route('home') }}?model=mg5" class="remove-item" type="button"><i class="far fa-times"></i></a>
                    <div class="card-image">
                        <img src="{{ asset('img/app/MG_5-Alpha-Carbon Gray.png') }}" alt="">
                    </div>
                    <div class="card-title">
                        <span class="model">MG 6</span>
                        <span>AT Alpha</span>
                    </div>
                </div>
            </div>
            <div class="grid-item" data-id="1">
                <div class="card-item">
                    <a href="{{ route('home') }}?model=mg5" class="remove-item" type="button"><i class="far fa-times"></i></a>
                    <div class="card-image">
                        <img src="{{ asset('img/app/MG_5-Alpha-Carbon Gray.png') }}" alt="">
                    </div>
                    <div class="card-title">
                        <span class="model">MG 6</span>
                        <span>AT Alpha</span>
                    </div>
                </div>
            </div>
            <div class="grid-item" data-id="2">
                <div class="card-item">
                    <a href="{{ route('home') }}?model=mg5" class="remove-item" type="button"><i class="far fa-times"></i></a>
                    <div class="card-image">
                        <img src="{{ asset('img/app/MG_5-Alpha-Carbon Gray.png') }}" alt="">
                    </div>
                    <div class="card-title">
                        <span class="model">MG 6</span>
                        <span>AT Alpha</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="compare-table">
            <div class="table-row table-field-row">
                <div class="table-h">Price</div>
                <div class="table-col">PHP 658,888.00</div>
                <div class="table-col">PHP 1,068,888.00</div>
                <div class="table-col">PHP 1,108,888.00</div>
            </div>
            <div class="table-row table-field-row">
                <div class="table-h">Power</div>
                <div class="table-col">400 PS</div>
                <div class="table-col">300 PS</div>
                <div class="table-col">350 PS</div>
            </div>
            <div class="table-row footer">
                <div class="table-h"></div>
                <div class="table-col">
                    <button class="btn btn-primary">Inquire</button>
                </div>
                <div class="table-col">
                    <button class="btn btn-primary">Inquire</button>
                </div>
                <div class="table-col">
                    <button class="btn btn-primary">Inquire</button>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="compare-actions">
    <div class="container">
        <form action="#">
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="EMAIL ADDRESS">
                <button type="submit" class="form-control-button btn btn-default">Send Email</button>
            </div>
            <a href="#" class="btn btn-default">Print</a>
        </form>
    </div>
</section>
@endsection