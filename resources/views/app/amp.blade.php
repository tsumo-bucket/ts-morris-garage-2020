<!DOCTYPE html>
<html amp>
    <head>
        <!-- The charset definition must be the first child of the `<head>` tag. -->
        <meta charset="utf-8">
        <title> Hello World</title>
        <!-- The AMP runtime must be loaded as the second child of the `<head>` tag.-->
        <script async src="https://cdn.ampproject.org/v0.js"></script>
        <!--
            AMP HTML files require a canonical link pointing to the regular HTML. If no HTML version exists, it should point to itself.
        -->
        <link rel="canonical" href="https://amp.dev/documentation/examples/introduction/hello_world/index.html">
        <!--
            AMP HTML files require a viewport declaration. It's recommended to include initial-scale=1.
        -->
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
        <!--
            CSS must be embedded inline.
        -->
        <style amp-custom>
            h1 {
            color: red;
            }
        </style>
        <!--
            The AMP boilerplate.
        -->
        <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

    </head>
    <body>
    <div class="title-bar"><i class="fas fa-times"></i></div>
  <!--
    Most HTML tags can be used directly in AMP HTML.
  -->
  <h1>Lorem Ipsum Dolor Sit Amet</h1>
  <!--
    Certain tags, such as the `<img>` tag, are replaced with equivalent or slightly enhanced custom AMP HTML tags (see [HTML Tags in the specification](https://github.com/ampproject/amphtml/blob/master/spec/amp-html-format.md)). You can use the [AMP Validator](/documentation/guides-and-tutorials/learn/validation-workflow/validate_amp) to check
    if your AMP HTML file is valid AMP HTML. Simply add `#development=1` to an AMP URL. Validation errors will be printed in the Javascript console. You can try it with this website which is built with AMP.

    Check out the [other examples](/documentation/examples/) to learn more about AMP.
  -->
  <amp-img src="{{asset($test->path)}}" width="500" height="610" layout="responsive"></amp-img>

</body>
</html>