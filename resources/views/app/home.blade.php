@extends('layouts.app')

@section('content')
    <section class="banner banner-full" style="background-image: url({{ asset('img/app/1920x1000_header.png') }});">
        <div class="container">
            <div class="banner-content-container">
                <div class="logo-img-container">
                    <img src="{{ asset('img/app/Click n Drive.png') }}" alt="">
                </div>
                <div class="banner-content-texts">
                    <h5>Get your new MG in just a few clicks!</h5>
                    <p>With MG Shop n' Ride, you can compare and select vehicles online,</p>
                    <p>contact our Sales Consultant for an e-presentation,</p>
                    <p>and secure your new vehicle through an online reservation.</p>
                    <h6>Follow these three easy steps to get your dream MG car!</h6>
                </div>
                <div class="banner-content-steps">
                    <div class="step-item">
                        {{-- <div class="count">1</div> --}}
                        <div class="icon">
                            <img src="{{ asset('img/app/Group 167.svg') }}" alt="">
                        </div>
                        <div class="text">Select<br>your MG</div>
                    </div>
                    <div class="step-item">
                        {{-- <div class="count">2</div> --}}
                        <div class="icon">
                            <img src="{{ asset('img/app/Group 168.svg') }}" alt="">
                        </div>
                        <div class="text">Chat with an<br>MG Sales Consultant</div>
                    </div>
                    <div class="step-item">
                        {{-- <div class="count">3</div> --}}
                        <div class="icon">
                            <img src="{{ asset('img/app/Group 169.svg') }}" alt="">
                        </div>
                        <div class="text">Reserve<br>your MG</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="cars-list">
        <div class="container">
            <div class="filter-container">
                <div class="heading">
                    <span class="icon"></span>
                    Search Vehicles
                </div>
                <form action="" class="filter-form">
                    <div class="grid-list">
                        <div class="grid-item">
                            <div class="form-group form-group-alt dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropDownModel"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Model
                                </button>
                                <div class="form-options dropdown-menu" aria-labelledby="dropDownModel">
                                    <div class="form-option-item dropdown-item active" data-value="MG 5"><i
                                            class="far fa-square"></i> MG 5</div>
                                    <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                            class="far fa-square"></i> MG 6</div>
                                    <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                            class="far fa-square"></i> MG ZS</div>
                                    <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                            class="far fa-square"></i> MG RX5 </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item">
                            <div class="form-group form-group-alt dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropDownYear"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Model Year
                                </button>
                                <div class="form-options dropdown-menu" aria-labelledby="dropDownYear">
                                    <div class="form-option-item dropdown-item active" data-value="MG 5"><i
                                            class="far fa-square"></i> 2020</div>
                                    <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                            class="far fa-square"></i> 2019</div>
                                    <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                            class="far fa-square"></i> 2018</div>
                                    <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                            class="far fa-square"></i> 2017</div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item">
                            <div class="form-group form-group-alt dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropDownColosr"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Available colors
                                </button>
                                <div class="form-options dropdown-menu" aria-labelledby="dropDownColosr">
                                    <div class="form-option-item dropdown-item active" data-value="MG 5"><i
                                            class="far fa-square"></i> Red</div>
                                    <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                            class="far fa-square"></i> Gray</div>
                                    <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                            class="far fa-square"></i> Black</div>
                                    <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                            class="far fa-square"></i> Yellow</div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item grid-item-button">
                            <button class="btn btn-primary">Search</button>
                        </div>
                    </div>
                    <div class="other-options hidden">
                        <div class="grid-list">
                            <div class="grid-list grid-list-1col">
                                <div class="grid-item">
                                    <div class="form-group form-group-alt dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropDownModel"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Transmissioon
                                        </button>
                                        <div class="form-options dropdown-menu" aria-labelledby="dropDownModel">
                                            <div class="form-option-item dropdown-item active" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG 5</div>
                                            <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG 6</div>
                                            <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG ZS</div>
                                            <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG RX5 </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-item">
                                    <div class="form-group form-group-alt dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropDownModel"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Seats
                                        </button>
                                        <div class="form-options dropdown-menu" aria-labelledby="dropDownModel">
                                            <div class="form-option-item dropdown-item active" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG 5</div>
                                            <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG 6</div>
                                            <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG ZS</div>
                                            <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG RX5 </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-list grid-list-1col">
                                <div class="grid-item">
                                    <div class="form-group form-group-alt dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropDownModel"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Engine Size
                                        </button>
                                        <div class="form-options dropdown-menu" aria-labelledby="dropDownModel">
                                            <div class="form-option-item dropdown-item active" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG 5</div>
                                            <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG 6</div>
                                            <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG ZS</div>
                                            <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG RX5 </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-item">
                                    <div class="form-group form-group-alt dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropDownModel"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Drive
                                        </button>
                                        <div class="form-options dropdown-menu" aria-labelledby="dropDownModel">
                                            <div class="form-option-item dropdown-item active" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG 5</div>
                                            <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG 6</div>
                                            <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG ZS</div>
                                            <div class="form-option-item dropdown-item" data-value="MG 5"><i
                                                    class="far fa-square"></i> MG RX5 </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-list grid-list-1col">
                                <div class="grid-item">
                                    <div class="price-filter">
                                        <div class="range-selector">
                                            <label>Price</label>
                                            <input type="text" id="price-range" class="range-select" value=""
                                                data-slider-min="600000" data-slider-max="4000000" data-slider-step="100"
                                                data-slider-value="600000" data-slider-id="RC" id="R"
                                                data-slider-tooltip="hide" />
                                            <div class="price-ranges">
                                                <span>PHP 600,000</span>
                                                <span>PHP 4,000,000</span>
                                            </div>
                                        </div>
                                        <div class="range-actions">
                                            <a href="#" class="clear-price-range">Clear</a>
                                            <a href="{{ route('home') }}?price-start=600000&price-end=4000000">Apply</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-item grid-item-button">
                                <button class="btn btn-primary">Reset All</button>
                            </div>
                        </div>
                    </div>
                    <div class="show-more-options">
                        <a href="#">Show more options</a>
                    </div>
                </form>
            </div>
            @yield('list')
        </div>
    </section>
    <section class="section-site-map">
        <div class="grid-item mg-care"
            style="background-image: url({{ asset('img/app/MG_Website_MGCare-Banner_Black_960x400_B.png') }})">
            <img src="{{ asset('img/app/Image 2.png') }}" alt="" class="mg-care-img">
            <div class="mg-care-programs">
                <img src="{{ asset('img/app/mg-icon.png') }}" alt="">
                <img src="{{ asset('img/app/mg-icon.png') }}" alt="">
                <img src="{{ asset('img/app/mg-icon.png') }}" alt="">
                <img src="{{ asset('img/app/mg-icon.png') }}" alt="">
            </div>
            <p>Experience MG Signature Service Guaranteed through our MG CARE program</p>
            <p class="no-margin">
                <a href="#">Learn More</a>
            </p>
        </div>
        </div>
        <div class="grid-item"
            style="background-image: url({{ asset('img/app/MG_Website_FAQS-Banner_Black_960x400.png') }})">
            <h5><i class="fas fa-comments"></i> FAQs</h5>
            <p>Learn more about the Click n’ Drive shopping process from unit availability to returns. For more inquiries,
                contact us through our 24/7 MG Hotline (02) 5328-4664, social media platforms, or the My MG App.</p>
            <a href="{{ route('faq') }}">Learn more</a>
        </div>
        <div class="grid-item" style="background-image: url({{ asset('img/app/1920x1000_dealership.png') }});">
            <h5><i class="fas fa-map-marker-alt"></i> Find a Dealer</h5>
            <p>See which dealership is nearest you by checking our locations here.</p>
            <a href="#">Learn more</a>
        </div>
    </section>
@endsection
@section('added-scripts')
    <script>
        $(document).ready(function() {
            var range = $('#price-range').slider({
                    range: true,
                    values: [600000, 4000000]
                })
                .on('slide', priceRangeChanged)
                .data('slider');

            $('.clear-price-range').on('click', function(e) {
                e.preventDefault();
                $("#price-range").attr('data-slider-value', [600000, 4000000]);
                $("#price-range").slider('refresh');
                priceRangeChanged();
            });

            $('.show-more-options a').on('click', function(e) {
                e.preventDefault();
                $('.other-options').toggleClass('hidden');
            });

            function priceRangeChanged() {
                const value = range.getValue();
                $('.price-ranges span:first-child').html('PHP ' + formatMoney(value[0], 0));
                $('.price-ranges span:last-child').html('PHP ' + formatMoney(value[1], 0));
            }

            function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
                try {
                    decimalCount = Math.abs(decimalCount);
                    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                    const negativeSign = amount < 0 ? "-" : "";

                    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                    let j = (i.length > 3) ? i.length % 3 : 0;

                    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(
                        /(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i)
                        .toFixed(decimalCount).slice(2) : "");
                } catch (e) {
                    console.log(e);
                }
            };
        });

    </script>
@endsection
