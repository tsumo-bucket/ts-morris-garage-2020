<div class="caboodle-form-group">
  <label for="name">Name</label>
  {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Name', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="email">Email</label>
  {!! Form::text('email', null, ['class'=>'form-control', 'id'=>'email', 'placeholder'=>'Email', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="contact_number">Contact Number</label>
  {!! Form::text('contact_number', null, ['class'=>'form-control', 'id'=>'contact_number', 'placeholder'=>'Contact Number', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="address">Address</label>
  {!! Form::textarea('address', null, ['class'=>'form-control redactor', 'id'=>'address', 'placeholder'=>'Address', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
<div class="caboodle-form-group">
  <label for="message">Message</label>
  {!! Form::textarea('message', null, ['class'=>'form-control redactor', 'id'=>'message', 'placeholder'=>'Message', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
<div class="caboodle-form-group">
  <label for="subject">Subject</label>
  {!! Form::text('subject', null, ['class'=>'form-control', 'id'=>'subject', 'placeholder'=>'Subject', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="birthdate">Birthdate</label>
  {!! Form::text('birthdate', null, ['class'=>'form-control sumodate', 'id'=>'birthdate', 'placeholder'=>'Birthdate', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="newsletter">Newsletter</label>
  {!! Form::text('newsletter', null, ['class'=>'form-control', 'id'=>'newsletter', 'placeholder'=>'Newsletter', 'required']) !!}
</div>
