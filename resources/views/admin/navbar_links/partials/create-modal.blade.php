<div class="modal fade" id="createNavLink" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="createNavLinkLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createNavLinkLabel">Add Navbar Link</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route'=>'adminNavbarLinksStore', 'method' => 'post', 'class'=>'form form-parsley form-create']) !!}
            <div class="modal-body">
                @include('admin.navbar_links.form')
            </div>
            <div class="modal-footer">
                <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-dismiss="modal">Cancel</button>
                <button type="submit" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>