<div class="modal fade" id="editNavLink" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="editNavLinkLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editNavLinkLabel">Edit Navbar Link</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::model($data, ['route'=>['adminNavbarLinksUpdate', 0], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit', 'id'=>'editLinkForm']) !!}
            <div class="modal-body">
                @include('admin.navbar_links.form')
            </div>
            <div class="modal-footer">
                <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-dismiss="modal">Cancel</button>
                <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>