@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
    <ol class="breadcrumb">
        <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item far active"><span>Menu</span></li>
    </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        @if (count($data) > 0)
        <a class="caboodle-btn caboodle-btn-large caboodle-btn-danger mdc-button mdc-ripple-upgraded mdc-button--unelevated hidden" 
                data-mdc-auto-init="MDCRipple"
                href="{{ route('adminNavbarLinksDestroy') }}"
                method="DELETE"
                data-toggle-alert="warning"
                data-alert-form-to-submit=".form-delete"
                permission-action="delete"
                data-notif-message="Deleting...">
            Delete
        </a>
        &nbsp;
        &nbsp;
        @endif
        <button
            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
            data-mdc-auto-init="MDCRipple"
            data-toggle="modal" data-target="#createNavLink"
        >
            Create
        </button>
    </div>
</header>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="caboodle-card">
            <div class="caboodle-card-body">
                @if (count($data) > 0)
                {!! Form::open(['route'=>'adminNavbarLinksDestroy', 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
                <div class="table-responsive">
                    <div class="container-fluid no-margin no-padding">
                        <div class="category-tree-table row sortable" sortable-data-url="{{route('adminNavbarLinksOrder')}}">
                            @foreach($categories as $category)
                                {{General::traverseCategoryTree($category)}}
                            @endforeach
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
                @else
                <div class="empty text-center">
                    No results found
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@include('admin.navbar_links.partials.create-modal') 
@include('admin.navbar_links.partials.edit-modal') 
@stop
@section('added-scripts')
<script>
    $(document).ready(function() {

        $("input[name='ids[]'").on('change', function (e) {
            $('.caboodle-btn-danger').toggleClass('hidden');
        });

        function toggleFormOptions(type) {
            if(type == 'external') {
                $('.internal').hide();
                $('.external').show();
            } else {
                $('.internal').show();
                $('.external').hide();
            }
        }
        
        function getOptions(url) {
            console.log(url);
            $.ajax({
                type : 'get',
                url: url,
                dataType: 'json',
                success : function(response) {
                    var data = [];
                    $('#reference_id').val(null).trigger('change');
                    $('#reference_id').empty();
                    _.each(response.data, function(key, value) {
                        $('#reference_id').append($("<option></option>")
                        .attr("value", value).text(key));
                    })
                },
                error : function(data, text, error) {
                    console.log(data);
                }
            });
        }

        $('#reference_type').on('change', function(e) {
            if($(this).val() == "App\\Article") {
                var url = "{{route('adminArticlesGet')}}";
            } else if($(this).val() == "App\\Page") {
                var url = "{{route('adminPagesGet')}}";
            }
            getOptions(url);
        });
           
        $("input[type='radio']").on('change', function (e) {
            toggleFormOptions($(this).val());
        });

        $("input[type='checkbox'].options").on('change', function (e) {
            if($(this).prop('checked')) {
                $(this).val(1);
            }
            else {
                $(this).val(0);
            }
        });
        
        $('.modal').on('hide.bs.modal', function (event) {
            var modal = $(this);
            $('form')[0].reset();
        });

        $('#createNavLink').on('show.bs.modal', function (event) {
            var option = $("input[type='radio']").val();
            toggleFormOptions(option);
        });

        $('#editNavLink').on('show.bs.modal', function (event) {
            $('.select2').select2();

            var button = $(event.relatedTarget); // Button that triggered the modal
            var id = button.data('id');
            var name = button.data('name');
            var type = button.data('type');
            var link = button.data('link');
            var hide = button.data('hide');
            var new_tab = button.data('new_tab');
            var disable_link = button.data('disable_link');
            var reference_type = button.data('reference_type');
            var reference_id = button.data('reference_id');
            var parent_id = button.data('parent_id');
            var modal = $(this);
            
            toggleFormOptions(type);
            $('#' + type).prop('checked', true);
            $('#editLinkForm').attr("action", "{{ url('/admin/navbar_links') }}" + "/" + id);
         
            modal.find('.modal-body input#id').val(id);
            modal.find('.modal-body input#name').val(name);
            modal.find('.modal-body input#link').val(link);
            modal.find('.modal-body input#hide').val(hide);
            modal.find('.modal-body #reference_type').val(reference_type);
            modal.find('.modal-body input#new_tab').val(new_tab);
            modal.find('.modal-body input#disable_link').val(disable_link);
            modal.find('.modal-body input#parent_id').val(parent_id);

            
            if(reference_type == "App\\Article") {
                var url = "{{route('adminArticlesGet')}}";
                getOptions(url);
            } else if(reference_type == "App\\Page") {
                var url = "{{route('adminPagesGet')}}";
                getOptions(url);
            }
            
            $('#editNavLink #reference_id').val(reference_id).trigger('change');

            if(type == 'internal') {
                modal.find('.modal-body input#internal').prop('checked',true);
            } else {
                modal.find('.modal-body input#external').prop('checked',true);
            }

            if(hide) {
                modal.find('.modal-body input#hide').prop('checked',true);
            } else {
                modal.find('.modal-body input#hide').prop('checked',false);
            }
            if(new_tab) {
                modal.find('.modal-body input#new_tab').prop('checked',true);
            } else {
                modal.find('.modal-body input#new_tab').prop('checked',false);
            }
            if(disable_link) {
                modal.find('.modal-body input#disable_link').prop('checked',true);
            } else {
                modal.find('.modal-body input#disable_link').prop('checked',false);
            }

        });

        $('.modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var parent_id = button.data('parent_id');
            var modal = $(this);
            if(parent_id) {
                modal.find('.modal-body input#parent_id').val(parent_id);
            }
        });

        $(document).on("click",".category-title",function(e){
            e.stopPropagation();

            if($(">.child-collapse",this).hasClass("in")) {
                $(".child-collapse",this).removeClass("in");
                $(".category-title",this).removeClass("in");
            } else {
                $(">.child-collapse",this).toggleClass("in");
            }

            $(this).toggleClass("in");
        });

        $(document).on("click",".collapse-all",function(e){
            e.stopPropagation();
            var category_title = $(this).closest('.category-title');

            if($(".child-collapse",category_title).hasClass("in")) {
                $(".child-collapse",category_title).removeClass("in");
                $(".category-title",category_title).removeClass("in");
            } else {
                $(".child-collapse",category_title).toggleClass("in");
            }
           
        });

        $(document).on('click', 'a', function(e) {
            e.stopPropagation();
        });

        $('body').on('change', '.delete-box input', function(e) {
            var target = $(this).closest('.category-details');
            var input = target.find('.category-title input[name="ids[]"]').prop('checked', $(this).prop("checked")); 
        });
    });
</script>
@stop