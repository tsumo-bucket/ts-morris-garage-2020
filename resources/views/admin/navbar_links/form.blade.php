<div class="row form-group">
    <div class="col-4">
        <div class="mdc-form-field">
            <div class="mdc-radio">
                <input class="mdc-radio__native-control" type="radio" id="internal" value="internal" name="type" checked>
                <div class="mdc-radio__background">
                    <div class="mdc-radio__outer-circle"></div>
                    <div class="mdc-radio__inner-circle"></div>
                </div>
                <div class="mdc-radio__ripple"></div>
            </div>
            <label for="internal">Internal</label>
        </div>
    </div>
    <div class="col-4">
        <div class="mdc-form-field">
            <div class="mdc-radio">
                <input class="mdc-radio__native-control" type="radio" id="external" name="type" value="external">
                <div class="mdc-radio__background">
                <div class="mdc-radio__outer-circle"></div>
                <div class="mdc-radio__inner-circle"></div>
                </div>
                <div class="mdc-radio__ripple"></div>
            </div>
            <label for="external">External</label>
        </div>
    </div>
</div>
<div class="caboodle-form-group">
    <label for="name">Name</label>
    {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Name', 'required']) !!}
</div>
<div class="caboodle-form-group external">
    <label for="link">Link</label>
    {!! Form::text('link', null, ['class'=>'form-control', 'id'=>'link', 'placeholder'=>'Link']) !!}
</div>
<div class="caboodle-form-group internal">
    <label for="reference_type">Reference Type</label>
    {!! Form::select('reference_type', ['App\Page' => 'Page', 'App\Article' => 'Article'], null, ['class'=>'form-control', 'id'=>'reference_type']) !!}
</div>    
<div class="caboodle-form-group internal">
    <label for="reference_id">Reference</label>
    {!! Form::select('reference_id', $pages, null, ['class'=>'form-control', 'id'=>'reference_id']) !!}
</div>
<div class="form-group">
    <label for="">Other Options</label>
</div>
<div>
    <div class="mdc-form-field">
        <div class="mdc-checkbox">
            <input type="checkbox" class="options mdc-checkbox__native-control" id="hide" name="hide" value="0" />
            <div class="mdc-checkbox__background">
                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                </svg>
                <div class="mdc-checkbox__mixedmark"></div>
            </div>
        </div>
        <label for="hide">Hide link</label>
    </div>
</div>
<div>
    <div class="mdc-form-field">
        <div class="mdc-checkbox">
            <input type="checkbox" class="options mdc-checkbox__native-control" id="new_tab" name="new_tab" value="0" />
            <div class="mdc-checkbox__background">
                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                </svg>
                <div class="mdc-checkbox__mixedmark"></div>
            </div>
        </div>
        <label for="new_tab">Open link in new tab</label>
    </div>
</div>
<div class="mdc-form-field">
    <div class="mdc-checkbox">
        <input type="checkbox" class="options mdc-checkbox__native-control" id="disable_link" name="disable_link" value="0" />
        <div class="mdc-checkbox__background">
            <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
            </svg>
            <div class="mdc-checkbox__mixedmark"></div>
        </div>
    </div>
    <label for="disable_link">Disable link</label>
</div>
<div class="caboodle-form-group">
    <!-- <label for="parent_id">Parent</label> -->
    {!! Form::hidden('parent_id', null, ['class'=>'form-control', 'id'=>'parent_id', 'placeholder'=>'Parent', 'readonly']) !!}
</div>