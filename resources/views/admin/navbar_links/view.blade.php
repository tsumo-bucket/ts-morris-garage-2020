@extends('layouts.admin')

@section('breadcrumbs')
<ol class="breadcrumb">
  <li><a href="{{route('adminDashboard')}}">Dashboard</a></li>
  <li><a href="{{route('adminNavbarLinks')}}">Navbar Links</a></li>
  <li class="active">View</li>
</ol>
@stop

@section('content')
<div class="col-md-8">
	<table class='table table-striped table-bordered table-view'>
		<tr>
			<th>Id</th>
			<td>{!!$data->id!!}</td>
		</tr>
		<tr>
			<th>Name</th>
			<td>{!!$data->name!!}</td>
		</tr>
		<tr>
			<th>Slug</th>
			<td>{!!$data->slug!!}</td>
		</tr>
		<tr>
			<th>Link</th>
			<td>{!!$data->link!!}</td>
		</tr>
		<tr>
			<th>Reference type</th>
			<td>{!!$data->reference_type!!}</td>
		</tr>
		<tr>
			<th>Reference id</th>
			<td>{!!$data->reference_id!!}</td>
		</tr>
		<tr>
			<th>Hide</th>
			<td>{!!$data->hide!!}</td>
		</tr>
		<tr>
			<th>New tab</th>
			<td>{!!$data->new_tab!!}</td>
		</tr>
		<tr>
			<th>Parent id</th>
			<td>{!!$data->parent_id!!}</td>
		</tr>
		<tr>
			<th>Created at</th>
			<td>
				@if ($data->created_at)
				<?php $created_at = new Carbon($data->created_at); ?>
				{{$created_at->toFormattedDateString() . ' ' . $created_at->toTimeString()}}
				@endif
			</td>
		</tr>
		<tr>
			<th>Updated at</th>
			<td>
				@if ($data->updated_at)
				<?php $created_at = new Carbon($data->updated_at); ?>
				{{$created_at->toFormattedDateString() . ' ' . $created_at->toTimeString()}}
				@endif
			</td>
		</tr>
	</table>
</div>
@stop