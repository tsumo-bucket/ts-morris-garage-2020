@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li  class="breadcrumb-item far">
            <a href="{{route('adminDashboard')}}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <span>Dev Modules</span>
        </li>
        <li class="breadcrumb-item far">
        <a href="{{route('adminPages')}}">Pages</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminPagesEdit', $page->id) }}">{{ $page->name }}</a>
        </li>
        <li class="breadcrumb-item far active">
        <span>Edit Content</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    @if(!@$data->seo)
        <div class="header-actions">
            <a href="{{ route('adminPageContents') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
            <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
        </div>
    @endif
</header>
@stop

<?php
// @section('content')
//     <div class="row">
//         @if(@$data->seo)
//         <div class="col-sm-8">
//             <div class="caboodle-card">
//                 <div class="caboodle-card-body">
//                     {!! Form::model($data, ['route'=>['adminPageContentsUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit']) !!}
//                     @include('admin.dev.page_contents.form')
//                     {!! Form::close() !!}
//                     <footer>
//                         <div class="text-right">
//                             <a href="{{ route('adminPageContents') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
//                             <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
//                         </div>
//                     </footer>
//                 </div>
//             </div>
//         </div>
//         <div class="col-sm-4">
// 			<div class="caboodle-card">
//                 <div class="caboodle-card-header">
//                 <h4 class="no-margin"><i class="far fa-globe"></i> SEO</h4>
//                 </div>
// 				<div class="caboodle-card-body">
// 					<div class="seo-url" data-url="{{route('adminPageContentsSeo')}}">
// 						@include('admin.seo.form')
// 					</div>
// 				</div>
// 			</div>
// 		</div>
//         @else
//         <div class="col-sm-12">
//             <div class="caboodle-card">
//                 <div class="caboodle-card-header">
//                 <h4 class="no-margin"><i class="far fa-file-alt"></i> FORM</h4>
//                 </div>
//                 <div class="caboodle-card-body">
//                 {!! Form::model($data, ['route'=>['adminPageContentsUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit']) !!}
//                 @include('admin.page_contents.form')
//                 {!! Form::close() !!}
//                 </div>
//             </div>
//         </div>
//         @endif
//     </div>
// @stop
?>
@section('footer')
<footer>
    <div class="text-right">
        <a class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple" href="{{ route('adminPagesEdit', $page->id) }}">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save Changes</button>
    </div>
</footer>
@stop
@section('content')
  {!! Form::model($data, ['route'=>['adminPageContentsUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit form-clear']) !!}
    @include('admin.dev.page_contents.form')
  {!! Form::close() !!}
@stop

@section('added-scripts')
    @include('admin.dev.page_controls.scripts')
@stop