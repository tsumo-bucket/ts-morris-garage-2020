@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{route('adminUsers')}}">Users</a>
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            <span>{{ $title }}</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('adminUsers') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</header>
@stop

@section('footer')
<footer>
    <div class="text-right">
        <a href="{{ route('adminUsers') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</footer>
@stop

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="caboodle-card">
      <div class="caboodle-card-header">
        <h4 class="no-margin">FORM</h4>
      </div>
      <div class="caboodle-card-body">
      {!! Form::model($user, ['route'=>['adminUsersUpdatePassword', $user->id], 'method' => 'patch', 'class'=>'form form-parsley']) !!}
      <!-- <div class="form-group">Name: {{$user->name}}</div>
      <div class="form-group">Email: {{$user->email}}</div> -->

      <div class="caboodle-form-group">
        <label for="new">New password</label>
        {!! Form::password('new', ['class'=>'form-control', 'id'=>'new', 'placeholder'=>'', 'required', 'data-parsley-minlength'=>'6']) !!}
      </div>
      <div class="caboodle-form-group">
        <label for="new_confirmation">Re-type new password</label>
        {!! Form::password('new_confirmation', ['class'=>'form-control', 'id'=>'new_confirmation', 'placeholder'=>'', 'required', 'data-parsley-equalto'=>'#new', 'data-parsley-minlength'=>'6']) !!}
      </div>
      <div class="form-group">
        {!! Form::checkbox('send_email', 'check') !!}
        <label for="send_email">Send {{$user->name}} an email regarding password change?</label>
      </div>
      {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@stop

@section('content')
<div class="col-sm-8">
  <div class="widget">
    <div class="header">
      <div>
        <i class="fa fa-file"></i> Form
      </div>
    </div>
  </div>
  
</div>
@stop