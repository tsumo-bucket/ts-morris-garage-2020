<!--
 Copyright 2016 Google Inc.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

<!doctype html>
<html amp lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
  @yield('head')
  <title>{{@$seo->title}}</title>
  <meta name="description" content="{{@$seo->description}}">
  <meta name="keywords" content="{{@$seo->keywords}}">
  <meta name="robots" content="index,follow">

  <meta property="og:title" content="{{@$seo->title}}">
  <meta property="og:description" content="{{@$seo->description}}">
  <meta property="og:image" content="{{ asset(@$seo->asset->path) }}">

  <meta name="twitter:title" content="{{@$seo->title}}">
  <meta name="twitter:description" content="{{@$seo->description}}">
  <meta name="twitter:image" content="{{ asset(@$seo->asset->path) }}">

  <style amp-custom>
    :root {
      --color-error: #B00020;
      --color-primary: #005AF0;
    }
    body {
      width: auto;
      margin: 0;
      padding: 0;
      color: #333333;
      font-size: 14px;
    }
    .amp-content-container {
      margin: 50px 0;
    }
    .container {
      max-width: 600px;
      margin: 0 auto;
      padding: 0px 15px;
    }

    article h1{
        font-size: 30px;
        margin-bottom: 20px;
        text-align: center;
    }

    article .details {
        margin-bottom: 30px;
        text-align: center
    }

    article .details span {
        font-size: 12px;
        text-transform: uppercase;
    }

    article .banner {
        text-align: center;
        margin-bottom: 30px;
    }

    article .content {
        line-height: 2;
    }

    article .content p {
        margin: 0 0 10px;
    }

    nav.navbar {
      margin-bottom: 50px;
      border: 1px solid transparent;
    }

    nav.navbar .container {
      height: 105px;
    }
    nav.navbar .nav-container {
      border-bottom: 1px solid #ddd;
    }

    .navbar .logo-container {
      float: left;
      padding: 15px 15px;
      font-size: 18px;
      line-height: 20px;
    }

    .navbar .navbar-toggle {
      position: relative;
      float: right;
      margin-right: 15px;
      padding: 9px 10px;
      margin-top: 8px;
      margin-bottom: 8px;
      background-color: transparent;
      background-image: none;
      border: 1px solid #ddd;
      border-radius: 4px;
      margin-top: 35px;
      cursor: pointer;
      outline: none;
    }

    .navbar .navbar-toggle .sr-only {
      position: absolute;
      width: 1px;
      height: 1px;
      padding: 0;
      margin: -1px;
      overflow: hidden;
      clip: rect(0, 0, 0, 0);
      border: 0;
    }

    .navbar .navbar-toggle .icon-bar {
      display: block;
      width: 22px;
      height: 2px;
      border-radius: 1px;
      background-color: #888;
    }
    .navbar-toggle .icon-bar + .icon-bar {
      margin-top: 4px;
    }

    a, a:hover, a:focus {
      text-decoration: none;
      color: inherit;
    }


    [class*="amphtml-sidebar-mask"] {
      background: rgba(0,0,0,0.3);
    }

    /* sidebar style start */

    .nav-sidebar {
      width: 250px;
    }
    
    .nav-sidebar .navs {
      color: #333;
      letter-spacing: 1px;
      text-transform: uppercase;
      font-size: 10px;
      padding: 20px 0;
      width: 250px;
      text-align: center;
    }

    /* sidebar style end */

    


  </style>
  
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
  <!-- <script async custom-element="amp-mega-menu" src="https://cdn.ampproject.org/v0/amp-mega-menu-0.1.js"></script> -->
  <script async custom-element="amp-font" src="https://cdn.ampproject.org/v0/amp-font-0.1.js"></script>
  <script async src="https://cdn.ampproject.org/v0.js"></script>
  <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
</head>

<body>
  <amp-font layout="nodisplay"
    font-family="montserratlight"
    timeout="2000"
    on-error-remove-class="montserrat-light-loading"
    on-error-add-class="montserrat-light-missing"
    on-load-remove-class="montserrat-light-loading"
    on-load-add-class="montserrat-light-loaded">
  </amp-font>
  <amp-font layout="nodisplay"
    font-family="montserratregular"
    timeout="2000"
    on-error-remove-class="montserrat-regular-loading"
    on-error-add-class="montserrat-regular-missing"
    on-load-remove-class="montserrat-regular-loading"
    on-load-add-class="montserrat-regular-loaded">
  </amp-font>
  <header>
    <nav class="navbar">
      <div class="nav-container">
        <div class="container">
          <a href="{{route('appHome')}}" class="logo-container">
            <amp-img src="{{asset('img/app/logo.png')}}" width="128" height="71" alt="{{$blog->name}}"></amp-img>
          </a>
          <button class="navbar-toggle" on="tap:sidebar.toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
      </div>
    </nav>
  </header>
  <!-- sidebar menu -->
  <amp-sidebar id="sidebar" class="nav-sidebar" side="left" layout="nodisplay">
    <div class="nav pull-right">
        <ul>
            @foreach(General::get_menu() as $menu)
                @if(!$menu->hide)
                    @if(count($menu->children) > 0)
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#collapse-{{$menu->id}}">
                                {{$menu->name}}
                                <i class="far fa-chevron-down"></i>
                            </a>
                            <ul class="collapse-list collapse no-icon" id="collapse-{{$menu->id}}">
                                @foreach($menu->children as $child)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{$child->type == 'external' ? $child->link:$child->page->slug}}">{{$child->name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    @else
                        <li class="nav-item active">
                            <a class="nav-link" href="{{$menu->type == 'external' ? $menu->link:$menu->page->slug}}">{{$menu->name}}</a>
                        </li>
                    @endif
                @endif
            @endforeach
        </ul>
    </div>
  </amp-sidebar>
  
  <div class="amp-content-container">
  <div class="container">
    @yield('content')
  </div>
  </div>
  @include('partials.footer.footer-amp')
</body>
</html>
