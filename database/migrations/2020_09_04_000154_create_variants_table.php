<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variants', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('model_id');
            $table->string('name');
            $table->string('image');
            $table->string('image_thumbnail')->nullable();
            $table->mediumText('short_description');
            $table->date('year_model');
            $table->string('transmission');
            $table->integer('engine_size');
            $table->integer('seats');
            $table->string('drive');
            $table->float('price')->unsigned();
            $table->enum('published', ['draft', 'published']);    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variants');
    }
}
