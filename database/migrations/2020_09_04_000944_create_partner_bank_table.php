<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnerBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_bank', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('image');
            $table->string('image_thumbnail')->nullable();
            $table->enum('published', ['draft', 'published']);    
            $table->timestamps();
            $table->softDeletes(); 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_bank');
    }
}
