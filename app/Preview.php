<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preview extends Model
{
    protected $fillable = [
    	'slug',
    	'value',
    	'status',
    	'action',
    	'reference_type',
    	'reference_id'
    ];

	public function reference()
	{
		return $this->morphTo();
	}
}
