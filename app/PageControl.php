<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class PageControl extends BaseModel
{
    
    protected $fillable = [
    	'reference_id',
    	'reference_type',
    	'name',
    	'label',
    	'type',
    	'options_json',
    	'required',
    	'order',
    	'value',
    	];
    
    
    public function reference()
    {
        return $this->morphTo();
	}
	
	public function asset()
	{
		return $this->hasOne('App\Asset', 'id', 'value');
	}
	public function video()
	{
		return $this->hasOne('App\VideoBanner', 'id', 'value');
	}

	// public function products()
	// {
	// 	return $this->belongsToMany("App\Product");
	// }
}
