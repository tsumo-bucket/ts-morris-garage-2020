<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'contact_number' => 'required|max:255',
            'address' => 'required',
            'message' => 'required',
            'subject' => 'required|max:255',
            'birthdate' => 'required',
            'order' => 'required|integer',
            'newsletter' => 'required|integer',
        ];
    }
}
