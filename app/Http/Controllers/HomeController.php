<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use Acme\Facades\Seo;
use App\Sample;
use App\Preview;
use App\Page;
use App\PageContent;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $input = $request->all();
        $list = [0, 1, 2, 3, 4, 5];
        if (@$input['model']) {
            return view('app/products/list')
                ->with('list', $list)
                ->with('request', $input);
        } else {
            return view('app/products/categories')
                ->with('list', $list)
                ->with('request', $input);
        }
    }

    public function compare(Request $request)
    {
        $input = $request->all();
        return view('app/compare')
            ->with('request', $input);
    }
    public function faq(Request $request)
    {
        $input = $request->all();
        return view('app/faq')
            ->with('request', $input);
    }

    public function preview($slug = '')
    {
        $preview = Preview::with('reference')->where('slug', $slug)->first();
        $preview_content = json_decode($preview->value, true);
        $content = PageContent::select('page_id')->findOrFail($preview->reference_id);
        $page = Page::findOrFail($content->page_id);

        if (view()->exists($page->slug)) {
            $view_name = 'app/' . $page->slug;
        } else {
            $view_name = 'app/home';
        }

        return view($view_name)
            ->with('data', $preview_content);
    }

    public function preview($slug='') 
    {
        $preview = Preview::with('reference')->where('slug', $slug)->first();
        $preview_content = json_decode($preview->value, true);
        $content = PageContent::select('page_id')->findOrFail($preview->reference_id);
        $page = Page::findOrFail($content->page_id);
        
        if (view()->exists($page->slug)) {
            $view_name = 'app/' . $page->slug;
        } else {
            $view_name = 'app/home';
        }
        
        return view($view_name)
        ->with('data', $preview_content);
    }

    public function seo()
    {
        $sample = Sample::findOrFail(1);
        $seo = Seo::get($sample);

        return view('app/home');
    }
}
