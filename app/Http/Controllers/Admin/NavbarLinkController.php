<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\NavbarLinkRequest;

use Acme\Facades\Activity;
use Acme\Facades\General;
use App\NavbarLink;
use App\Seo;
use App\Page;
use App\Article;


class NavbarLinkController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        $data = NavbarLink::where("parent_id","0")->orderBy('order','asc');

        if ($name = $request->name) {
            $data = $data->where('name', 'LIKE', $name . '%');
        }

        $data = $data->get();
        if(!$data->isEmpty())
        {
            foreach ($data as $key => $parent) {
                $parent->children = $this->getCategoryTree($parent->id);
                $categories[] = $parent;
            }

        } else {
            $categories = [];
        }


        $_pages = Page::select('name','id','published')->get();
        $pages = [];
        foreach($_pages as $key => $value) {
            $pages[$value->id] = $value->name . ' ('. $value->published .')'; 
        }

        return view('admin/navbar_links/index')
            ->with('title', 'Menu')
            ->with('menu', 'navbar_links')
            ->with('keyword', $request->name)
            ->with('data', $data)
            ->with('categories', $categories)
            ->with('pages', $pages);
    }

    public function create()
    {
        return view('admin/navbar_links/create')
            ->with('title', 'Create navbar link')
            ->with('menu', 'navbar_links');
    }
    
    public function store(NavbarLinkRequest $request)
    {
        $input = $request->all();
        if ($input['parent_id'] === null) {
            $input['parent_id'] = 0;
        }
        $navbar_link = NavbarLink::create($input);
        
		// $input['slug'] = General::slug($navbar_link->name,$navbar_link->id);
		// $navbar_link->update($input);

        // $log = 'creates a new navbar_link "' . $navbar_link->name . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful.',
            'resetForm'=>true,
            'redirect'=>route('adminNavbarLinks')
        ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/navbar_links/show')
            ->with('title', 'Show navbar link')
            ->with('data', NavbarLink::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/navbar_links/view')
            ->with('title', 'View navbar link')
            ->with('menu', 'navbar_links')
            ->with('data', NavbarLink::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = NavbarLink::findOrFail($id);
        $seo = $data->seo()->first();

        return view('admin/navbar_links/edit')
            ->with('title', 'Edit navbar link')
            ->with('menu', 'navbar_links')
            ->with('data', $data)
            ->with('seo', $seo);
    }

    public function order(Request $request)
    {  
   
        $input=[];
        $data = $request->input('navlink');
        $newOrder=1;
        foreach($data as $d)
        {
            $input['order'] = $newOrder;
            $navlink = NavbarLink::findOrFail($d);
            $navlink->update($input);
            $newOrder++;
        }

        $response = [
            'notifTitle'=>'Navbar link order updated.',
        ];
        return response()->json($response);
    }
    
    public function update(NavbarLinkRequest $request, $id)
    {
        $input = $request->all();
        $navbar_link = NavbarLink::findOrFail($id);
        $navbar_link->update($input);

        // $log = 'edits a navbar_link "' . $navbar_link->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }


    public function seo(Request $request)
    {
        $input = $request->all();

        $data = NavbarLink::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = NavbarLink::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->name;
        }
        // $log = 'deletes a new navbar_link "' . implode(', ', $names) . '"';
        // Activity::create($log);

        NavbarLink::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminNavbarLinks')
        ];

        return response()->json($response);
    }

    public function getCategoryTree($id){

        $category_children = NavbarLink::where("parent_id",$id)->get();
        $children = [];

        if(!$category_children->isEmpty())
        {
            foreach ($category_children as $key => $value) {
                $value->children = $this->getCategoryTree($value->id);

                $children[] = $value;
            }
        }

        return $children;
    }
    public function traverseCategories(Request $request)
    {
        $parents = NavbarLink::where("parent_id","0")->get();
        $response = [];
        foreach($parents as $key => $category) {
            $response[$key] = General::traverseCategoryTree($category, true);
        }

        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/navbar_links', array('as'=>'adminNavbarLinks','uses'=>'Admin\NavbarLinkController@index'));
Route::get('admin/navbar_links/create', array('as'=>'adminNavbarLinksCreate','uses'=>'Admin\NavbarLinkController@create'));
Route::post('admin/navbar_links/', array('as'=>'adminNavbarLinksStore','uses'=>'Admin\NavbarLinkController@store'));
Route::get('admin/navbar_links/{id}/show', array('as'=>'adminNavbarLinksShow','uses'=>'Admin\NavbarLinkController@show'));
Route::get('admin/navbar_links/{id}/view', array('as'=>'adminNavbarLinksView','uses'=>'Admin\NavbarLinkController@view'));
Route::get('admin/navbar_links/{id}/edit', array('as'=>'adminNavbarLinksEdit','uses'=>'Admin\NavbarLinkController@edit'));
Route::patch('admin/navbar_links/{id}', array('as'=>'adminNavbarLinksUpdate','uses'=>'Admin\NavbarLinkController@update'));
Route::post('admin/navbar_links/seo', array('as'=>'adminNavbarLinksSeo','uses'=>'Admin\NavbarLinkController@seo'));
Route::delete('admin/navbar_links/destroy', array('as'=>'adminNavbarLinksDestroy','uses'=>'Admin\NavbarLinkController@destroy'));
*/
}
