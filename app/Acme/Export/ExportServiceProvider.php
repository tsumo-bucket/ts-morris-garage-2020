<?php

namespace App\Acme\Export;

use Illuminate\Support\ServiceProvider;

class ExportServiceProvider extends ServiceProvider 
{
	public function register()
	{
		$this->app->bind('export', 'App\Acme\Export\Export');
	}
}