<?php 

namespace App\Acme\General;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;

use App\Option;
use App\Email;
use App\Page;
use App\NavbarLink;
use Mail;
use Carbon;

class General extends Mail
{	

	public function get_site_option($slug){
		$option = Option::whereSlug($slug)->first();
		$response = ($option) ? $option->value : '';
		return $response;
    }

    public function get_menu() {
        $data = NavbarLink::where("parent_id","0")->orderBy('order','asc')->get();
        
        if(!$data->isEmpty())
        {
            foreach ($data as $key => $parent) {
                if($parent->type == "internal") {
                    if($parent->reference_type && $parent->reference_id) {
                        $parent->page = $parent->get_related($parent->reference_type,$parent->reference_id);
                    }
                }
                
                $parent->children = $this->get_children($parent->id);
                $response[] = $parent;
            }
        } else {
            $response = [];
        }

        return $response;
    }

    public function get_children($id) {
        $category_children = NavbarLink::where("parent_id",$id)->get();
        $children = [];

        if(!$category_children->isEmpty())
        {
            foreach ($category_children as $key => $value) {
                if($value->type == "internal") {
                    if($value->reference_type && $value->reference_id) {
                        $value->page = $value->get_related($value->reference_type,$value->reference_id);
                    }
                }
                $value->children = $this->get_children($value->id);

                $children[] = $value;
            }
        }

        return $children;
    }
     
	public function get_store_pages () {
		$pages = Page::all();

		return $pages;
    }
    
	public function saveTranslations($input, $translatables, $model, $reference_id) {
		$languages = $this->getLanguages();
		if(is_numeric($languages)) {
			$languages = [];
		}
		//-- remove old translations
		$old_translations = Translation::where('reference_type', $model)->where('reference_id', $reference_id)->pluck('id')->toArray();
		Translation::destroy($old_translations);

		if($translatables && (count($input) > 0) && $model) {
			$translatables = explode(',' , $translatables);

			foreach($languages as $language) {
				foreach($translatables as $translatable) {
					if(@$input[$translatable . '_' . $language->slug]) {
						$translation = [
							"language_id" => $language->id,
							"reference_type" => $model,
							"reference_id" => $reference_id,
							"field_name" => $translatable,
							"value" => $input[$translatable . '_' . $language->slug]
						];
	
						Translation::create($translation);
					}
				}
			}
		}
    }
    
	public function get_ga_code(){
		$option = Option::whereSlug('analytics-script')->first();
		$response = ($option) ? $option->value: 'TBD';
		return $response;
	}
	
	public function slug($value,$id)
	{
		$slug = str_slug($value."-".$id);
		return $slug;
	}

	public function checkPermission($children,$ids,$curParentId,$data){
		foreach($children as $c){
			if($c->parent==0){

				if(count($c->children)>0){
					echo '<li><input type="checkbox" id="func-'.$c->id.'" name="ids[]" value="'.$c->id.'" '.(in_array($c->id,$data) ? "checked":"").'><label for="func-'.$c->id.'" class="parent header">';
					echo $c->name;
					echo '</label></li>';
					echo '<ul class="parent permission-list">';
					$this->checkPermission($c->children,$ids,$c->id,$data);
					echo '</ul>';

				}else{
					echo '<li><input type="checkbox" name="ids[]" value="'.$c->id.'" '.(in_array($c->id,$data) ? "checked":"").'><label class="parent header">';
					echo $c->name;
					echo '</label></li>';
				}
			}
			else if(!in_array($c->id,$ids)){

				if(count($c->children)>0){
					echo '<li><input type="checkbox" id="func-'.$c->id.'" name="ids[]" value="'.$c->id.'" '.(in_array($c->id,$data) ? "checked":"").'><label for="func-'.$c->id.'" class="parent header">';
					echo $c->name;
					echo '</label></li>';
					echo '<ul class="permission-list">';
					$this->checkPermission($c->children,$ids,$c->id,$data);
					echo '</ul>';

				}else{
					echo '<li><input type="checkbox"  id="func-'.$c->id.'" name="ids[]" name="ids[]" value="'.$c->id.'" '.(in_array($c->id,$data) ? "checked":"").'><label for="func-'.$c->id.'" class="parent header">';
					echo $c->name;
					echo '</label></li>';
				}
			}
			else if(in_array($c->id,$ids) && $c->parent==$curParentId){

				if(count($c->children)>0){
					echo '<li><input type="checkbox" id="func-'.$c->id.'" name="ids[]" value="'.$c->id.'" '.(in_array($c->id,$data) ? "checked":"").'><label for="func-'.$c->id.'" class="parent header">';
					echo $c->name;
					echo '</label></li>';
					echo '<ul class="permission-list">';
					$this->checkPermission($c->children,$ids,$c->id,$data);
					echo '</ul>';

				}else{
					echo '<li><input type="checkbox" id="func-'.$c->id.'" name="ids[]" value="'.$c->id.'" '.(in_array($c->id,$data) ? "checked":"").'><label for="func-'.$c->id.'" class="parent header">';
					echo $c->name;
					echo '</label></li>';
				}
				continue;

			}
		}

	}
    
    public function traverseCategoryTree($category,$isBrowse = false)
	{
        $child_indicator = $category->parent_id == "0" ? "" : "&#8627;";
        $open_new_tab = $category->new_tab == "1" ? "opens in new tab":"";
        $hidden = $category->hide == "1" ? "hidden":"";
        $disabled = $category->disable_link == "1" ? "link disabled":"";
        $link_type = $category->type;
        $category_str = <<<EOF
	    	<div class="col-sm-12 category-details" sortable-id="navlink-$category->id">
EOF;
		echo $category_str;

		if(!$isBrowse)
        {
		    $category_str = <<<EOF
		            <div class="delete-box">
		              	<div class="form-box-checkbox">
							<div class="mdc-checkbox margin-auto">
								<input class="mdc-checkbox__native-control" name="ids[]" type="checkbox" value="$category->id"/>
								<div class="mdc-checkbox__background">
  									<svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
										<path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
  									</svg>
  									<div class="mdc-checkbox__mixedmark"></div>
								</div>
							</div>
		                </div>
		            </div>
EOF;
			echo $category_str;
		}
		else if($category->parent_id != "0")
		{
		    $category_str = <<<EOF
		            <div class="delete-box">
		              	$child_indicator
		            </div>
EOF;
			echo $category_str;
		}
		

	    $category_str = <<<EOF
	            <div class="category-title" data-id="$category->id">
	            	<div class="flex-container">
EOF;
		echo $category_str;

		if(!$isBrowse)
		{
		    $category_str = <<<EOF
	            		<div class="child-indicator">
		            		$child_indicator
		            	</div>
EOF;
			echo $category_str;
		}

		$category_str = <<<EOF
						<div class="title">
							
EOF;
		echo $category_str;

		$category_str = <<<EOF
                    <div>
                        <div class="name">$category->name</div>
                        <div class="details">
                            <small class="type">$category->type</small>
                            <small class="new_tab">$open_new_tab</small>
                            <small class="hidden">$hidden</small>
                            <small class="disable">$disabled</small>

EOF;
        echo $category_str;
        if($category->type="external") {
            $category_str = <<<EOF
                <small class="link">$category->link</small>
            </div>
        </div>
        <div class="actions caboodle-flex-right">


EOF;
echo $category_str;

        } else {
            $category_str = <<<EOF
                <small class="link">$category->name</small>
            </div>
        </div>
        <div class="actions caboodle-flex-right">

EOF;
echo $category_str;

        }

        if(count($category->children) > 0)
        {

            $category_str = <<<EOF
                    <a role="button" class="drop-down collapse-all animated-icon" data-toggle="tooltip" title="Collapse all">
		                <i class="far fa-chevron-circle-down"></i>
                    </a>
                    
EOF;
			echo $category_str;

        }

		if(!$isBrowse)
		{
			$category_str = <<<EOF
				            	<a 
                                    class=animated-icon edit-link" 
                                    data-toggle="modal" data-target="#editNavLink"
                                    data-id="$category->id"
                                    data-name="$category->name"
                                    data-link="$category->link"
                                    data-type="$link_type"
                                    data-reference_type="$category->reference_type"
                                    data-reference_id="$category->reference_id"
                                    data-parent_id="$category->parent_id"
                                    data-hide="$category->hide"
                                    data-new_tab="$category->new_tab"
                                    data-disable_link="$category->disable_link"
                                    role="button"
                                    aria-pressed="false"
                                    permission-action="edit">
                                    <i class="far fa-edit" aria-hidden="true"></i>
                                </a>
                                <a 
                                    class="animated-icon add-child" 
                                    data-toggle="modal" data-target="#createNavLink"
                                    data-parent_id="$category->id"
                                    role="button"
                                    aria-pressed="false"
                                    permission-action="create">
                                    <i class="far fa-plus" aria-hidden="true"></i>
                                </a>
                            </div>
EOF;
			echo $category_str;
		}

		$category_str = <<<EOF
			            </div>
		            </div>
		            <div class="no-padding child-collapse">
EOF;
		echo $category_str;

        if(count($category->children) > 0)
        {
            foreach ($category->children as $key => $value) {
                $this->traverseCategoryTree($value, $isBrowse);
            }   
        }

        $category_str = <<<EOF
        			</div>
	            </div>
         </div>
EOF;

	echo $category_str;

        return;
	}
    
	public function getIGPosts()
	{
		//needs to create instagram-feed-token in general settings
		// optional instagram-feed-size default value is 8
		$access_token = OptionModel::where('slug','instagram-feed-token')->first();
		
		if ($access_token) {
			
			try {
				
				$endpoint = "https://api.instagram.com/v1/users/self/media/recent/";
				$client = new \GuzzleHttp\Client();
				$limit = OptionModel::where('slug','instagram-feed-size')->first();
				$response = $client->get($endpoint, [ 'query' => [
					'access_token'=> @$access_token->value,
					'count'=> ($limit)? $limit->value:8
				]]);
					
					
				$statusCode = $response->getStatusCode();
				$content = json_decode($response->getBody(), true);
				
				return $content['data'];
				//returns arrays of array not object.
					
					
			} catch (RequestException $e) {
				return null;
			} catch (\Exception $e) {
				return null;
			}

		} else {
			return null;
		}
				
	}
}